<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
Use App\Models\Website;

class WebsiteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Website::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few Website in our database:
        for ($i = 0; $i < 5; $i++) {
            Website::create([
                'title' => $faker->sentence,
            ]);
        }
    }
}

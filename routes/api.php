<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\API\SubscriberController;
use App\Http\Controllers\API\PostController;
use App\Http\Controllers\TestController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// just for request testing
Route::post('PariseTest', function(Request $request)
    {
        return $request->all();
    }
);

// All post related route
Route::get('post-list', [PostController::class, 'index']);
Route::post('post', [PostController::class, 'store']);
Route::get('post/{id}', [PostController::class, 'show']);
Route::post('post', [PostController::class, 'store']);
Route::put('post/{id}', [PostController::class, 'update']);
Route::delete('post/{id}', [PostController::class, 'delete']);


// user route
Route::get('subcriber-list', [SubscriberController::class, 'index']);
Route::post('subcriber', [SubscriberController::class, 'store']);

Route::post('send-email', [SubscriberController::class, 'sendEmail']);

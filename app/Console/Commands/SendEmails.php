<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
Use App\Models\Subscriber;
use Illuminate\Support\Facades\Mail;
Use App\Models\Post;
use App\Http\Resources\Post as PostResource;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:SendEmails {post}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'SendEmails to all subscriber';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $post_id = $this->argument('post'); // Post_id pass as argument after artisan command

        $postObject = Post::find($post_id);
   
        $website_id = $postObject['website_id'];        
        $Subscriber_list = Subscriber::where('website_id', $website_id)->get();
        

        foreach($Subscriber_list as $obj){
            $data = [
                'no-reply' => 'no-reply@companydomain.com',
                'Fname'    => $obj->fname,
                'Lname'    => $obj->lname,
                'Email'    => $obj->email,
                'Post_body' => $postObject['body'],
                'Post_title' => $postObject['title'],
            ];
    
            Mail::send('email_contact', ['data' => $data],
                function ($message) use ($data)
                {
                    $message
                        ->from($data['no-reply'])
                        ->to($data['Email'])->subject('New Aticle notification');
                });
    
        }        
    }
}

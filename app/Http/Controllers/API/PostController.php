<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
Use App\Models\Post;
use Validator;
use App\Http\Resources\Post as PostResource;

class PostController extends BaseController
{
    // List of all post
    public function index()
    {
        $post_list = Post::all();
        return $this->sendResponse(PostResource::collection($post_list), 'Post retrieved successfully.');
    }

    // view a post
    public function show($id)
    {
        $post = Post::find($id);
  
        if (is_null($post)) {
            return $this->sendError('Post not found.');
        }
   
        return $this->sendResponse(new PostResource($post), 'Post retrieved successfully.');
    }

    // Create new post
    public function store(Request $request)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'title' => 'required|unique:posts|max:255',
            'body' => 'required',
            'status' => 'required',
            'website_id' => 'required'
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $post = Post::create($input);
   
        return $this->sendResponse(new PostResource($post), 'Post created successfully.');
        
    }

    // update old post
    public function update(Request $request, Post $post)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'title' => 'required',
            'body' => 'required'
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $post->title = $input['title'];
        $post->body = $input['body'];
        $post->status = $input['status'];
        $post->website = $input['website'];
        $post->save();
   
        return $this->sendResponse(new PostResource($post), 'Post updated successfully.');
    }

    // delete post
    public function delete(Post $post)
    {
        $post->delete();   
        return $this->sendResponse([], 'Post deleted successfully.');
    }
}

<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
Use App\Models\Subscriber;
use Validator;
use App\Http\Resources\Subscriber as SubscriberResource;
use Illuminate\Support\Facades\Mail;
Use App\Models\Post;
use App\Http\Resources\Post as PostResource;


class SubscriberController extends BaseController
{
    // List of all Subscriber
    public function index()
    {
        $sub_list = Subscriber::all();
        return $this->sendResponse(SubscriberResource::collection($sub_list), 'Subscriber retrieved successfully.');
    }

    // Create new Subscriber
    public function store(Request $request)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'email' => 'required|unique:subscribers',
            'fname' => 'required',
            'lname' => 'required',
            'website_id' => 'required'
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $sub = Subscriber::create($input);
   
        return $this->sendResponse(new SubscriberResource($sub), 'Subscriber created successfully.');
        
    }

    public function sendEmail(Request $request)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'post_id' => 'required'
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $post_id    = $input['post_id'];  
        $postObject = Post::find($post_id);
   
        $website_id = $postObject['website_id'];        
        $Subscriber_list = Subscriber::where('website_id', $website_id)->get();
        

        foreach($Subscriber_list as $obj){
            $data = [
                'no-reply' => 'no-reply@companydomain.com',
                'Fname'    => $obj->fname,
                'Lname'    => $obj->lname,
                'Email'    => $obj->email,
                'Post_body' => $postObject['body'],
                'Post_title' => $postObject['title'],
            ];
    
            Mail::send('email_contact', ['data' => $data],
                function ($message) use ($data)
                {
                    $message
                        ->from($data['no-reply'])
                        ->to($data['Email'])->subject('New Aticle notification');
                });
    
        }        

        return $this->sendResponse(new PostResource($postObject), 'Email Send successfully.');        
    }

    
}
